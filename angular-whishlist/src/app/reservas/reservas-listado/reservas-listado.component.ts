import { Component, OnInit } from '@angular/core';
import { ResercasApiClientService } from '../resercas-api-client.service';

@Component({
  selector: 'app-reservas-listado',
  templateUrl: './reservas-listado.component.html',
  styleUrls: ['./reservas-listado.component.css']
})
export class ReservasListadoComponent implements OnInit {

  constructor(public api: ResercasApiClientService) { }

  ngOnInit(): void {
  }

}
