import { TestBed } from '@angular/core/testing';

import { ResercasApiClientService } from './resercas-api-client.service';

describe('ResercasApiClientService', () => {
  let service: ResercasApiClientService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ResercasApiClientService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
